import React, { useState } from "react";
import "./App.css";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { selectTables } from "./features/table/tableSlice";
import Table from "./features/table/Table";
import Reservation from "./features/reservation/Reservation";
import { addReservation, selectReservations } from "./features/reservation/reservationSlice";

function App() {
  const reservations = useAppSelector(selectReservations);
  const tables = useAppSelector(selectTables);
  const dispatch = useAppDispatch();
  const [reservationInput, setReservationInput] = useState("");

  const resetReservationInput = () => {
    setReservationInput("");
  }

  const handleAddReservation = () => {
    if(!reservationInput){
      return;
    }
    dispatch(addReservation(reservationInput));
    resetReservationInput();
  }

  return (
    <div className="App">
      <div className="container">
        <div className="reservation-container">
          <div>
            <h5 className="header">Reservations</h5>
            <div className="reservation-cards-container">
              {reservations.map((reservationName, index) => <Reservation key={index} name={reservationName} index={index}/>)}
            </div>
          </div>
          <div className="reservation-input-container">
            <input value={reservationInput} onChange={(event) => setReservationInput(event.target.value)}/>
            <button onClick={() => handleAddReservation()}>Add</button>
          </div>
        </div>
        <div className="customer-food-container">
          <h5 className="header">Tables</h5>
          {tables.map((table, index) => <Table key={index} index={index} table={table}/>)}
        </div>
      </div>
    </div>
  );
}

export default App;