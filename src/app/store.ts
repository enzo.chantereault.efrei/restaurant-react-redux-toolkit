import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import TableSlice from '../features/table/tableSlice';
import reservationSlice from '../features/reservation/reservationSlice';

export const store = configureStore({
  reducer: {
    reservations: reservationSlice,
    tables: TableSlice
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
