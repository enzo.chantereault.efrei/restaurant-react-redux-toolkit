import React from 'react'
import { useAppDispatch } from '../../app/hooks';
import { createTable } from '../table/tableSlice';
import { removeReservation } from './reservationSlice';

interface ReservationProps {
    name: string;
    index: number;
}

const Reservation = ({name, index}: ReservationProps) => {
    const dispatch = useAppDispatch();

    const handleAddTable = () => {
        dispatch(createTable(name));
        dispatch(removeReservation(index));
    }

    return (
        <div className="reservation-card-container" onClick={() => handleAddTable()}>{name}</div>
    )
};

export default Reservation;