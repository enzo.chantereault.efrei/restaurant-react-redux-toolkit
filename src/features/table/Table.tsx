import React, { useState } from 'react'
import { useAppDispatch } from '../../app/hooks';
import { addOrder, removeOrder, removeTable } from './tableSlice';

interface TableEntity {
    name: string; 
    orders: string[];
}

interface TableProps {
    index: number;
    table: TableEntity;
}

const Table = ({index, table}: TableProps) => {
    const dispatch = useAppDispatch();
    const [orderInput, setOrderInput] = useState("");

    const resetOrderInput = () => {
        setOrderInput("");
    }

    const handleAddOrder = () => {
        if(!orderInput){
        return;
        }
        dispatch(addOrder({tableIndex: index, order: orderInput}));
        resetOrderInput();
    }

    return (
        <div className="customer-food-card-container">
            <p onClick={() => dispatch(removeTable(index))}>{table.name}</p>
            <div className="customer-foods-container">
                <div className="customer-food">{table.orders.map((orderName: string, orderIndex: number) => <p key={orderIndex} onClick={() => dispatch(removeOrder({tableIndex: index, orderIndex: orderIndex}))}>{orderName}</p>)}</div>
                <div className="customer-food-input-container">
                    <input value={orderInput} onChange={(event) => setOrderInput(event.target.value)}/>
                    <button onClick={() => handleAddOrder()}>Add</button>
                </div>
            </div>
        </div>
    )
};

export default Table;