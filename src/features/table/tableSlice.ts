import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface OrderTableState {
    value: {
        name: string; 
        orders: string[];
    }[];
}
  
const initialState: OrderTableState = {
    value: []
};

export const TableSlice = createSlice({
    name: "tables",
    initialState,
    reducers: {
        createTable: (state, action: PayloadAction<string>) => {
            state.value.push({name: action.payload, orders: []});
        },
        removeTable: (state, action: PayloadAction<number>) => {
            state.value.splice(action.payload, 1);
        },
        addOrder: (state, action: PayloadAction<{tableIndex: number, order: string}>) => {
            state.value[action.payload.tableIndex].orders.push(action.payload.order);
        },
        removeOrder: (state, action: PayloadAction<{tableIndex: number, orderIndex: number}>) => {
            state.value[action.payload.tableIndex].orders.splice(action.payload.orderIndex, 1);
        }
    }
});

export const { createTable, removeTable, addOrder, removeOrder } = TableSlice.actions;
export const selectTables = (state: RootState) => state.tables.value;
export default TableSlice.reducer;